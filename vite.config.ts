import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import postcss from './postcss.config'
const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    resolve: {
	alias: [
	    { find: '@', replacement: path.resolve(__dirname, 'src') }
	]
    },
    css: {
	postcss
    }
})
