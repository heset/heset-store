import React, { useState } from "react";
import { MenuIcon } from "@heroicons/react/outline";
import { Link, NavLink } from "react-router-dom";

export interface NavItemProps {
  to: String;
  label: String;
}

export interface NavigationProps {
  routes: NavItemProps[];
  buttons: NavItemProps[];
}

const NavItemLink = function (props: NavItemProps) {
    return (
            <li className="px-4 py-2">
              <NavLink
                to={props.to}
                className="cursor-pointer underline transition-all hover:text-gray-700"
              >
                {props.label}
              </NavLink>
            </li>
    )
}

const NavItemButton = function (props: NavItemProps) {
   return (
            <li className="py-2">
              <NavLink
                to={props.to}
                className="cursor-pointer rounded-full border border-yellow-700 px-4 py-2 text-yellow-700 transition-all hover:bg-yellow-700 hover:text-white"
              >
                {props.label}
              </NavLink>
            </li>
   )
}

const NavItemMobile = function (props: NavItemProps) {
    return (
            <li className="px-4 py-2">
              <NavLink
                to={props.to}
                className="cursor-pointer underline transition-all hover:text-gray-700"
              >
                {props.label}
              </NavLink>
            </li>
    )
}

const Navigation = function (props: NavigationProps) {
  const [menuVisible, setMenuVisible] = useState(false);
  return (
    <div className="w-full bg-white">
      <nav className="flex justify-between border-b py-6">
        <NavLink to="/" className="px-4 py-2">
          Logo
        </NavLink>
        <ul className="flex hidden space-x-5 lg:inline-flex">
          {props.routes.map((nav, i) => (
            <NavItemLink {...nav} key={i}/>
          ))}
          {props.buttons.map((nav, i) => (
            <NavItemButton {...nav} key={i}/>
          ))}
        </ul>
        <button
          className="my-2 lg:hidden"
          onClick={() => setMenuVisible(!menuVisible)}
        >
          <MenuIcon className="h-6" />
        </button>
      </nav>
      {menuVisible ? (
        <ul className="lg:hidden">
          {props.routes.map((nav, i) => (
            <NavItemMobile {...nav} key={i} />
          ))}
          {props.buttons.map((nav, i) => (
            <NavItemMobile {...nav} key={i} />
          ))}
        </ul>
      ) : (
        <></>
      )}
    </div>
  );
};

export default Navigation;
