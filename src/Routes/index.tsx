import React, { lazy, ReactNode } from "react"
import { GlobeAltIcon, HomeIcon, LibraryIcon, ShoppingBagIcon } from '@heroicons/react/outline'

const Home = lazy(() => import('../Pages/Home'))
const Store = lazy(() => import('../Pages/Store'))
const Seller = lazy(() => import('../Pages/Seller'))
const Deliver = lazy(() => import('../Pages/Deliver'))
export interface Route {
    component: ReactNode,
    path: string,
    title: string,
    icon?: ReactNode
}

const routes: Route[] = [
    { component: Home, path: '/', title: 'Home', icon: HomeIcon },
    { component: Store, path: 'buy', title: 'Buy food', icon: ShoppingBagIcon },
    { component: Seller, path: 'register', title: 'Register an Store', icon: LibraryIcon },
    { component: Deliver, path: 'deliver', title: 'Deliver', icon: GlobeAltIcon }
]

export default routes