import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./Assets/Styles/tailwind.css";

import Navigation from "./Components/Navigation";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./Pages/Home";
import StorePage from "./Pages/Store";
import Seller from "./Pages/Seller";
import Deliver from "./Pages/Deliver";
import NotFound from "./Pages/NotFound";
import Orders from "./Pages/Orders";

ReactDOM.render(
  <BrowserRouter>
    <div className="container mx-auto">
      <Navigation
        routes={[
          { label: "Sell", to: "/sell" },
          { label: "Deliver", to: "/deliver" },
        ]}
        buttons={[{ label: "Orders", to: "/orders" }]}
      />
      <div className="py-6">
        <Suspense fallback={<div>Loading...</div>}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/store" element={<StorePage />}>
              <Route path=":storeId" element={<StorePage />} />
            </Route>
            <Route path="/sell" element={<Seller />} />
            <Route path="/deliver" element={<Deliver />} />
            <Route path="/orders" element={<Orders />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Suspense>
      </div>
      <footer className="bg-slate-700 p-4 text-white rounded">
        <div className="text-center">
          &copy; Heset {new Date().getFullYear()}
        </div>
      </footer>
    </div>
  </BrowserRouter>,
  document.getElementById("root")
);
