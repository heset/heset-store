import { SearchIcon } from '@heroicons/react/outline'
import React from 'react'
import cakeUrl from '@/Assets/Images/cake.jpg'
import banseUrl from "@/Assets/Images/banse.jpg";
import hawaiFoodUrl from "@/Assets/Images/hawai-food.jpg";
import lotsUrl from "@/Assets/Images/lots.jpg";
import dish2Url from "@/Assets/Images/dish-2.jpg";


const favoriteFoods = [{name: 'Awesome cupcake', description: 'A simple cupcake', imgUrl: cakeUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: banseUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: hawaiFoodUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: lotsUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: dish2Url},
		      {name: 'Awesome cupcake', description: 'A simple cupcake', imgUrl: cakeUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: banseUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: hawaiFoodUrl},
		       {name: 'Awesome banse', description: 'A simple banse', imgUrl: lotsUrl},
		      {name: 'Awesome banse', description: 'A simple banse', imgUrl: dish2Url}]

const foods = [{name: 'Normal cupcake', description: 'A just normal cupcake', imgUrl: cakeUrl},
	       {name: 'Normal cupcake', description: 'A just normal cupcake', imgUrl: banseUrl},
	       {name: 'Normal cupcake', description: 'A just normal cupcake', imgUrl: hawaiFoodUrl},
	       {name: 'Normal cupcake', description: 'A just normal cupcake', imgUrl: lotsUrl},
	      {name: 'Normal cupcake', description: 'A just normal cupcake', imgUrl: dish2Url}]

const Search = ({}) => {
    return (
        <div className='flex w-full border border-black/50 rounded-lg p-2 focus:outline-double focus:ring-emerald-200'>
            <input placeholder='search...' className='w-full focus:outline-none' />
            <button><SearchIcon className='h-6 w-6' /></button>
        </div>
    )
}


const Food = ({imgUrl, name, description, favorite = false}) => {
    return (
        <li className="cursor-pointer rounded-lg snap-center drop-shadow-lg bg-white">
	    <div className={"rounded-t-lg" + favorite ? "min-w-40" : "min-w-80"}>
		<img src={imgUrl} alt={name} className="h-full w-full object-cover object-center"/>
	    </div>
            <div className='p-5'>
                <h3 className='text-lg font-bold'>{name}</h3>
                <p className='text-black/80'>{description}</p>
            </div>
        </li>
    )
}

const FavoriteFoods = ({foods}) => {
    return (
        <ul className='w-full overflow-x-auto flex snap-x py-8 gap-5'>
            {foods.map((food) => (<Food {...food} favorite/>))}
        </ul>
    )
}

const Foods = ({foods}) => {
    return (
        <ul className='grid gap-6 grid-cols-1 lg:grid-cols-6'>
            {foods.map((foods) => (<Food {...foods} />))}
        </ul>
    )
}

const StorePage = ({}) => {
    return (
        <>
            <Search />
            <FavoriteFoods foods={favoriteFoods}/>
            <Foods foods={foods}/>
        </>
    )
}

export default StorePage
