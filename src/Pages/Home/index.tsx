import React from "react";
import illustrationUrl from "@/Assets/Images/food-delivery-vector-illustration3.png";
import { MenuIcon, SearchIcon } from "@heroicons/react/outline";
import banseUrl from "@/Assets/Images/banse.jpg";
import cakeUrl from "@/Assets/Images/cake.jpg";
import hawaiFoodUrl from "@/Assets/Images/hawai-food.jpg";
import lotsUrl from "@/Assets/Images/lots.jpg";
import dish2Url from "@/Assets/Images/dish-2.jpg";
import { Link } from "react-router-dom";

const stores = [
  {
    id: 1,
    name: "Spinka and Sons",
    description:
      "Vero nihil laboriosam impedit. Labore voluptatem quo mollitia nam voluptas velit deleniti. Ut fuga quam. Est consequatur non.",
    image: banseUrl,
  },
  {
    id: 5,
    name: "McCullough, Corkery and Crooks",
    description:
      "Nam a ut eum tempora voluptatibus molestias. Voluptatem id non vel provident nobis et. Sit veniam omnis. Dolorem libero necessitatibus numquam culpa ea dolores vel. Non numquam consequatur nam maxime delectus corporis voluptates dolores. Nisi sit est quis et veritatis mollitia ut quidem minima.",
    image: cakeUrl,
  },
  {
    id: 2,
    name: "Hettinger and Sons",
    description:
      "Ratione temporibus modi autem qui aut a voluptatum iste quo. Accusamus esse quae delectus modi asperiores. Aut cupiditate laborum cumque veniam nemo itaque eos aut consequuntur. Vel cumque aut est cum eius eaque ut.",
    image: hawaiFoodUrl,
  },
  {
    id: 3,
    name: "Hermann, Hickle and Schimmel",
    description:
      "Voluptatem sint cum. Nostrum in modi et eligendi vero voluptatibus aut. Sed laborum nam rerum quia vel incidunt culpa. Perspiciatis aut voluptas culpa nulla et expedita qui.",
    image: lotsUrl,
  },
  {
    id: 4,
    name: "Krajcik Inc",
    description:
      "At id laboriosam doloremque dignissimos. Voluptatem aperiam eveniet. Omnis asperiores ut ratione.",
    image: dish2Url,
  },
];

export default function Home() {
  return (
    <>
      <header className="relative h-[32rem]">
        <div className="h-full w-full">
          <img
            src={illustrationUrl}
            className="h-full w-full object-cover object-center opacity-20"
          />
        </div>
        <div className="absolute top-1/2 mx-auto my-auto flex h-full w-full flex-col content-center px-0 text-center">
          <h1 className="py-2 text-3xl font-extrabold text-gray-700 lg:text-7xl">
            Your food your way
          </h1>
          <p className="py-2 lg:text-lg">
            A complete food delivery system on the blockchain.
          </p>
          <div className="relative mx-auto my-2 inline flex max-w-3xl items-center justify-center rounded-full border bg-white px-4 py-2 text-gray-400 transition-all focus-within:border-yellow-700 focus-within:text-yellow-700 focus-within:ring-1 focus-within:ring-yellow-700">
            <input
              placeholder="search..."
              className="h-5 w-full pr-7 focus:placeholder-yellow-700 focus:outline-none"
            />
            <SearchIcon className="pointer-events-none absolute right-0 mr-3 h-5" />
          </div>
        </div>
      </header>
      <main>
        <div className="grid grid-cols-1 gap-6 py-6 lg:grid-cols-4">
          {stores.map((store) => (
            <Link
              to={`/store/${store.id}`}
              key={store.name}
              className="cursor-pointer rounded-lg border shadow transition-all hover:opacity-70"
            >
              <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-t-lg">
                <img
                  src={store.image}
                  alt={store.name}
                  className="h-full w-full object-cover object-center"
                />
              </div>
              <div className="p-4">
                <h3 className="text-lg font-bold text-gray-700">
                  {store.name}
                </h3>
                <p className="text-sm text-gray-500">{store.description}</p>
              </div>
            </Link>
          ))}
        </div>
      </main>
    </>
  );
}
